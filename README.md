# HowToModifySystemImage
> 对刷机包的system.img修改，然后重新打包
> 资源链接:https://gitee.com/chenjimcom/HowToModifySystemImage

### 1. 准备工作：
- 解压解打包工具，得到三个文件：make_ext4fs、mkuserimg.sh、simg2img，
- 把它们跟要修改的 *.img.ext4(或*.img)文件放置到同一个目录下

### 2. 转换源文件为"*.img.ext4"格式  
使用`./simg2img src des`命令来转换，eg:  
`./simg2img system.img system.img.ext4`
### 3. 挂载镜像
新建一个目录，挂载此img到这个目录上使用  
`sudo mkdir sysmain`  
`mount -t ext4 -o loop system.img.ext4 sysmain`

挂载成功后就可以在资源管理器中管理该img内的文件

### 4. 修改镜像内容

### 5. 重新打包
`chmod 777 ./make_ext4fs`
`./make_ext4fs -l 1610612736 -s -a system system_out.img ./sysmain`  
 其中`1610612736 `是分区大小  
